# AED Guías 2023

Repositorio con las guías de ejercicios y sus soluciones para el ciclo lectivo 2023


## Listado de Guías

- [Guía 01 - Fundamentos de Programación](./Guia%2001)
- [Guía 02 - Estructuras Secuenciales](./Guia%2002)
